#JAVA_HOME=/usr/lib/jvm/java-8-openjdk

all: build

install:
	mvn clean install

build:
	mvn clean package

build.skip.test
	mvn clean package -DskipTests

clean:
	mvn clean
	
test:
	mvn test
	
deploy:
	mvn clean deploy -Dgpg.passphrase=${MAVEN_CENTRAL_PASSPHRASE}