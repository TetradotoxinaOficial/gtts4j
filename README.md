# gTTS4J
gTTS4j (Google Text-to-Speech for Java). Convert text to speech using Google Translate results returning an mp3 file or you can manipulate the audio bits as well. When working with Google Translate the translation has also been integrated

## Social networks
* [Web](https://tetradotoxina.com)
* [Twitch](https://www.twitch.tv/tetradotoxina)
* [Youtube](https://www.youtube.com/channel/UCKGueVQMO7_YhifvFuRBb6g)
* [Fanpage Oficial (Entertainment)](https://bit.ly/3wHqavn)
* [Fanpage Developers (Developers)](https://bit.ly/3upIG9V)
* [Facebook Group (Developers)](https://bit.ly/3bTZaAQ)

## Disclaimer
This project is not affiliated with Google or Google Cloud. In the future there may be changes that make the library obsolete. 

## Import library
## _Maven_	
    <dependency>
        <groupId>com.tetradotoxina</groupId>
        <artifactId>gtts4j</artifactId>
        <version>2.0.0</version>
    </dependency>

## _Gradle_
    implementation 'com.tetradotoxina:gtts4j:2.0.0'


## _Text to Speech_
	GTTS4J gtts4j = new GTTS4JImpl();
	String text = "enter the text you want to convert to speech";
	String lang = "en";
	boolean slow = false;
	String filePath = System.getProperty("user.dir")+File.separator+"demo.mp3";
	
	byte[] data = gtts4j.textToSpeech(text, lang, slow);
	gtts4j.saveFile(filePath, data, true);

## _Translate_	
	GTTS4J gtts4j = new GTTS4JImpl();
	String text = "enter the text you want to translate";
	String lang = "en";
	String langTranslate = "es";
	
	String traslate = gtts4j.translate(text, lang, langTranslate);
	System.out.println("Translation: "+traslate);

## Supported Languages 

  * 'af' : 'Afrikaans'
  * 'sq' : 'Albanian'
  * 'ar' : 'Arabic'
  * 'hy' : 'Armenian'
  * 'bn' : 'Bengali'
  * 'ca' : 'Catalan'
  * 'zh' : 'Chinese'
  * 'hr' : 'Croatian'
  * 'cs' : 'Czech'
  * 'da' : 'Danish'
  * 'nl' : 'Dutch'
  * 'en' : 'English'
  * 'eo' : 'Esperanto'
  * 'fi' : 'Finnish'
  * 'fr' : 'French'
  * 'de' : 'German'
  * 'el' : 'Greek'
  * 'hi' : 'Hindi'
  * 'hu' : 'Hungarian'
  * 'is' : 'Icelandic'
  * 'id' : 'Indonesian'
  * 'it' : 'Italian'
  * 'ja' : 'Japanese'
  * 'ko' : 'Korean'
  * 'la' : 'Latin'
  * 'lv' : 'Latvian'
  * 'no' : 'Norwegian'
  * 'pl' : 'Polish'
  * 'pt' : 'Portuguese'
  * 'ro' : 'Romanian'
  * 'ru' : 'Russian'
  * 'sr' : 'Serbian'
  * 'si' : 'Sinhala'
  * 'sk' : 'Slovak'
  * 'es' : 'Spanish'
  * 'sw' : 'Swahili'
  * 'sv' : 'Swedish'
  * 'ta' : 'Tamil'
  * 'th' : 'Thai'
  * 'tr' : 'Turkish'
  * 'uk' : 'Ukrainian'
  * 'vi' : 'Vietnamese'
  * 'cy' : 'Welsh'
