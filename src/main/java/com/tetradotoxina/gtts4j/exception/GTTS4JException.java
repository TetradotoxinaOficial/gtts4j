package com.tetradotoxina.gtts4j.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GTTS4JException extends Exception {
	private static final long serialVersionUID = -6850821253091751859L;
	
	private String message;
	
	
	public GTTS4JException(String message) {
		super(message);
		this.message=message;
	}
}
