package com.tetradotoxina.gtts4j.model;

import com.tetradotoxina.gtts4j.configuration.GTTS4JConstant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BatchExecuteRequest {

	private String url;
	
	@Builder.Default
	private String userAgent = GTTS4JConstant.USER_AGENT;
	
	@Builder.Default
	private String contentType = GTTS4JConstant.HEADER_CONTENT_TYPE_FORM;
	private String fReqParam;
	private String atParam;
	
	private String cookie;
	
}
