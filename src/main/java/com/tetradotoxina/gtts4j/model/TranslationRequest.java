package com.tetradotoxina.gtts4j.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class TranslationRequest {

	private String atParam;
	private String cookie;
	
	private String fSid;
	private String bl;
	
	private String text;
	private String lang;
	private String langTranslate;
	
	@Builder.Default
	private Boolean slow = null;
	
	private String reqid;
		
	public String getSlowFormat() {
		return slow == null || slow == false ? "null" : "true";
	}
	
}
