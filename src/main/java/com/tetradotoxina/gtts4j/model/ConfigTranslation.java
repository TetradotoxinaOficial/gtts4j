package com.tetradotoxina.gtts4j.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ConfigTranslation {

	private String text;
	
	@Builder.Default
	private String lang = "auto";
	
	@Builder.Default
	private String langTranslate = "en"; 	
}
