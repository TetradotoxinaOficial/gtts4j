package com.tetradotoxina.gtts4j.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;

import com.tetradotoxina.gtts4j.GTTS4J;
import com.tetradotoxina.gtts4j.configuration.GTTS4JConstant;
import com.tetradotoxina.gtts4j.exception.GTTS4JException;
import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.service.TranslationService;
import com.tetradotoxina.gtts4j.service.impl.TraslateServiceImpl;

import lombok.extern.java.Log;

@Log
public class GTTS4JImpl implements GTTS4J {

	private TranslationService translationService;

	public GTTS4JImpl() {
		translationService = new TraslateServiceImpl();
	}

	public String translate(String text, String lang,String langTranslate) throws GTTS4JException{

		ConfigTranslation traslateConfig = ConfigTranslation.builder()
				.text(text)
				.lang(lang)
				.langTranslate(langTranslate)
				.build();
		
		return translate(traslateConfig);
	}

	@Override
	public byte[] textToSpeech(String text) throws GTTS4JException {

		return textToSpeech(text, GTTS4JConstant.G_LANG_DEFAULT,GTTS4JConstant.G_LANG_TRANSLATE_DEFAULT, false);

	}
	
	@Override
	public byte[] textToSpeech(String text, boolean slow) throws GTTS4JException {

		return textToSpeech(text, GTTS4JConstant.G_LANG_DEFAULT,GTTS4JConstant.G_LANG_TRANSLATE_DEFAULT, slow);

	}
	
	@Override
	public byte[] textToSpeech(String text,String lang) throws GTTS4JException{
		
		return textToSpeech(text, lang,GTTS4JConstant.G_LANG_TRANSLATE_DEFAULT, false);
	}
	
	@Override
	public byte[] textToSpeech(String text, String lang, boolean slow) throws GTTS4JException {

		return textToSpeech(text, lang,GTTS4JConstant.G_LANG_TRANSLATE_DEFAULT, slow);

	}
			
	@Override
	public byte[] textToSpeech(String text, String lang,String langTranslate, boolean slow) throws GTTS4JException {

		ConfigTranslation traslateConfig = ConfigTranslation.builder()
				.text(text)
				.lang(lang)
				.langTranslate(langTranslate)
				.build();
		
		return textToSpeech(traslateConfig);

	}
	
	@Override
	public byte[] textToSpeech(ConfigTranslation configTranslation) throws GTTS4JException {
			
		AttributeResponse attributeResponse = translationService.findAttributes(configTranslation);
			
		TranslationRequest translationRequest = TranslationRequest.builder()			
			.atParam(attributeResponse.getAtParam())
			.cookie(attributeResponse.getCookie())
			.fSid(attributeResponse.getFSid())
			.bl(attributeResponse.getBl())
			.text(configTranslation.getText().replaceAll("\n|\t|\r"," ").trim())
			.lang(configTranslation.getLang())
			.langTranslate(configTranslation.getLangTranslate())
			.reqid(attributeResponse.getReqid())
			.build();
		
		String response = translationService.textToSpeech(translationRequest);

		//log.info("[GTTS4JImpl][textToSpeech] response: "+response);
	
		return getBytes(response);
	}

	private byte[] getBytes(String response) throws GTTS4JException {
		
		String src = "";
		byte[] bytes = null;

		try {
			String index = "//";
			//String end = "\\\"]\",null,null,null,\"generic\"]]";
			String end = "\\";
			
						
			int beginIndex = response.indexOf(index);
			int endIndex = response.lastIndexOf(end);
					
			src = response.substring(beginIndex, endIndex).replaceAll("\\\\u200b|\\\\u003d|\\\\", "");				
			bytes = Base64.getDecoder().decode(src);

		} catch (Exception e) {
			log.severe("[GTTS4JImpl][getBytes] getBytes: "+response);
			log.severe("[GTTS4JImpl][getBytes] src: "+src);
			log.severe(e.getMessage());
			throw new GTTS4JException("Service response failed or file not found\n"+e.getMessage());
		}
				
		return bytes;

	}

	
	private String findTranslation(String response) throws GTTS4JException{
				
		//String index = "[[[null,null,null,null,null,[[\\\"";
		String index = "null,[[\\\"";
		String end = "\\\",null,null,null,[[";
		
					
		int beginIndex = response.indexOf(index)+index.length();
		int endIndex = response.lastIndexOf(end);
		
		String result = "";
				
		try {
			result = response.substring(beginIndex, endIndex).replaceAll("\\\\u200b", "");			
		}catch (Exception e) {
			log.severe("[GTTS4JImpl][findTranslation] response: "+response);
			log.severe(e.getMessage());
			throw new GTTS4JException("No result found: "+e.getMessage());
		}
		
		return result;

	}
	
	@Override
	public void saveFile(String filePath, byte[] bytes) throws GTTS4JException {
		saveFile(filePath, bytes,false);
	}

	@Override
	public void saveFile(String filePath, byte[] bytes, boolean overwriteFile ) throws GTTS4JException {
		
		File file = new File(filePath);
		if (file.exists() && overwriteFile) {
			file.delete();
		}
		
		try (OutputStream stream = new FileOutputStream(filePath)) {
		    stream.write(bytes);
		}catch (Exception e) {
			log.severe(e.getMessage());
			throw new GTTS4JException(e.getMessage());
		}		
		
	}

	@Override
	public String translate(ConfigTranslation configTranslation) throws GTTS4JException {
			
		AttributeResponse attributeResponse = translationService.findAttributes(configTranslation);
		
		TranslationRequest translationRequest = TranslationRequest.builder()			
				.atParam(attributeResponse.getAtParam())
				.cookie(attributeResponse.getCookie())
				.fSid(attributeResponse.getFSid())
				.bl(attributeResponse.getBl())
				.text(configTranslation.getText().replaceAll("\r\n|\n|\t|\r"," ").trim())
				.lang(configTranslation.getLang())
				.langTranslate(configTranslation.getLangTranslate())
				.reqid(attributeResponse.getReqid())
				.build();
		
		String response = translationService.translate(translationRequest);
		
		return findTranslation(response);
	}


}
