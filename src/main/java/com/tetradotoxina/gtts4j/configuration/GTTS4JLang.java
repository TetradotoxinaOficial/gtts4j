package com.tetradotoxina.gtts4j.configuration;

import lombok.Getter;
import lombok.ToString;
import java.util.Arrays;

@Getter
@ToString
public enum GTTS4JLang {
	
	AF("af" , "Afrikaans"),
	SQ("sq" , "Albanian"),
	AR("ar" , "Arabic"),
	HY("hy" , "Armenian"),
	BN("bn" , "Bengali"),
	CA("ca" , "Catalan"),
	ZH("zh" , "Chinese"),
	//ZH_CN("zh-cn" , "Chinese (Mandarin/China)"),
	//ZR_YUE("zh-yue" , "Chinese (Cantonese)"),
	HR("hr" , "Croatian"),
	CS("cs" , "Czech"),
	DA("da" , "Danish"),
	NL("nl" , "Dutch"),
	EN("en" , "English"),
	EO("eo" , "Esperanto"),
	FI("fi" , "Finnish"),
	FR("fr" , "French"),
	DE("de" , "German"),
	EL("el" , "Greek"),
	HI("hi" , "Hindi"),
	HU("hu" , "Hungarian"),
	IS("is" , "Icelandic"),
	ID("id" , "Indonesian"),
	IT("it" , "Italian"),
	JA("ja" , "Japanese"),
	//LM("km" , "Khmer (Cambodian)"),
	KO("ko" , "Korean"),
	LA("la" , "Latin"),
	LV("lv" , "Latvian"),
	//MK("mk" , "Macedonian"),
	NO("no" , "Norwegian"),
	PL("pl" , "Polish"),
	PT("pt" , "Portuguese"),
	RO("ro" , "Romanian"),
	RU("ru" , "Russian"),
	SR("sr" , "Serbian"),
	SI("si" , "Sinhala"),
	SK("sk" , "Slovak"),
	ES("es" , "Spanish"),
	SW("sw" , "Swahili"),
	SV("sv" , "Swedish"),
	TA("ta" , "Tamil"),
	TH("th" , "Thai"),
	TR("tr" , "Turkish"),
	UK("uk" , "Ukrainian"),
	VI("vi" , "Vietnamese"),
	CY("cy" , "Welsh");
	
	
	private String code;
	private String lang;	
	
	private GTTS4JLang(String code,String lang){
		this.code = code;
		this.lang = lang;
	}
	
	public GTTS4JLang findByCode(String code) {
		return Arrays.stream(GTTS4JLang.values()).filter(value -> value.getCode().equals(code)).findFirst().get();
	}
	
	public GTTS4JLang findByLang(String lang) {
		return Arrays.stream(GTTS4JLang.values()).filter(value -> value.getLang().equalsIgnoreCase(lang)).findFirst().get();
	}
}
