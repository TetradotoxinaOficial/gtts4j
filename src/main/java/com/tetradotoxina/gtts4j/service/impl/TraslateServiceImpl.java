package com.tetradotoxina.gtts4j.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.tetradotoxina.gtts4j.configuration.GTTS4JConstant;
import com.tetradotoxina.gtts4j.exception.GTTS4JException;
import com.tetradotoxina.gtts4j.model.AttributeResponse;
import com.tetradotoxina.gtts4j.model.BatchExecuteRequest;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;
import com.tetradotoxina.gtts4j.model.TranslationRequest;
import com.tetradotoxina.gtts4j.service.TranslationService;
import com.tetradotoxina.gtts4j.util.GTTS4JUtil;

import lombok.extern.java.Log;

@Log
public class TraslateServiceImpl implements TranslationService {
	
	
	@Override
	public String textToSpeech(TranslationRequest traslateRequest) throws GTTS4JException {

		String url = GTTS4JConstant.URL_BASE
				+ String.format(GTTS4JConstant.URL_BATCH_EXECUTE, 
						traslateRequest.getFSid(), 
						traslateRequest.getBl(),
						traslateRequest.getLang(), 
						traslateRequest.getReqid());
				
		String fReqParam = String.format(GTTS4JConstant.REQUEST_PARAM_F_REQ_FORMAT,
				GTTS4JUtil.fReqTextFormat(traslateRequest.getText()),
				traslateRequest.getLang(),
				traslateRequest.getSlowFormat());	
		//log.info("fReqParam: "+fReqParam);
		BatchExecuteRequest batchExecuteRequest=BatchExecuteRequest.builder()
				.url(url)
				.cookie(traslateRequest.getCookie())
				.atParam(traslateRequest.getAtParam())
				.fReqParam(fReqParam)
				.build();
				
		Response response = postBatchExecute(batchExecuteRequest);
		
		return response.body();
	}
	
	/*
	public String[] bigTextToSpeech(TranslationRequest traslateRequest) throws GTTS4JException{
		
		String url = GTTS4JConstant.URL_BASE
				+ String.format(GTTS4JConstant.URL_BATCH_EXECUTE, 
						traslateRequest.getFSid(), 
						traslateRequest.getBl(),
						traslateRequest.getLang(), 
						traslateRequest.getReqid());
				
		String fReqParam = String.format(GTTS4JConstant.REQUEST_PARAM_F_REQ_FORMAT,
				traslateRequest.getText(),
				traslateRequest.getLang(),
				traslateRequest.getSlowFormat());
				
		String cookie = traslateRequest.getCookie();
				
		String[] linesOfText  = findTextSeparator(traslateRequest.getText(),GTTS4JConstant.MAX_CHARS); 
		
		String[] Textresult  = Arrays.stream(linesOfText).map(lines -> {
			
			BatchExecuteRequest batchExecuteRequest=BatchExecuteRequest.builder()
					.url(url)
					.cookie(cookie)
					.atParam(traslateRequest.getAtParam())
					.fReqParam(fReqParam)
					.build();
					
			Response response = null;
			
			try {
				response = postBatchExecute(batchExecuteRequest);
			} catch (GTTS4JException e) {
				e.printStackTrace();
			}
			
			return response.body();			
			 
			
		}).toArray(String[]::new);
		
		return Textresult;
	}

	private String[] findTextSeparator(String text, int maxChars) throws GTTS4JException{
		
		
		if(text.length() <= maxChars) {
			return new String[] {text};
		}
						
		String [] split = text.split(" ");
		
		
		int length = split.length;
		
		double div = length/maxChars;
		int numberOfLines = (int) (div%2 == 0 ? div : div+1);
		
		String[] lines = new String[numberOfLines];

		int splitIndex = 0;
		
		for(int i=0 ;i<length;i++) {
			lines[i]="";			
			
			for(int f=0;f<maxChars;f++) {
				lines[i] = lines[i]+split[splitIndex];
				splitIndex++;
			}
			
		}
				
		return lines;
	}*/

	@Override
	public AttributeResponse findAttributes(ConfigTranslation configTranslation) throws GTTS4JException {
		
		if(configTranslation.getText()==null || configTranslation.getText().isEmpty() || configTranslation.getText().length()>GTTS4JConstant.MAX_CHARS) {
			throw new GTTS4JException("Empty text or has exceeded "+GTTS4JConstant.MAX_CHARS+" characters allowed");
		}
		
		
		String url = GTTS4JConstant.URL_BASE + String.format(GTTS4JConstant.URL_PATH, 
				configTranslation.getLang(),
				configTranslation.getLang(),
				configTranslation.getLangTranslate(), 
				//configTranslation.getText().replaceAll("\r\n|\n|\r","%0A").replaceAll("\\\\","").trim());
				configTranslation.getText().replaceAll("\r\n|\n|\r","%0A").replaceAll("\\\\","").replace("\"", "").trim());
		
		log.info("[TraslateServiceImpl][findAttributes] URL: "+url);
		
		Response response;
		try {
			response = Jsoup.connect(url)
					.userAgent(GTTS4JConstant.USER_AGENT)
					.ignoreContentType(true)
					.method(Connection.Method.GET)
					.followRedirects(true)
					.execute();
		} catch (IOException e) {
			log.severe(e.getMessage());
			throw new GTTS4JException(e.getMessage());
		}
			
		Document doc = Jsoup.parse(response.body());
		Elements scripts = doc.getElementsByTag("script");
		Map<String,String> params = findParameters(scripts);
		
		String cookie = response.cookie(GTTS4JConstant.COOKIE_NID);
		
		//String reqid="";
		
		return AttributeResponse.builder()
				.cookie(cookie)
				.atParam(params.get("at"))				
				.fSid(params.get("fsid"))
				.bl(configTranslation.getLangTranslate())
				//.reqid(reqid)
				.build();
	}
	
	private Map<String,String> findParameters(Elements scripts)throws GTTS4JException {
						
		Map<String,String> result = new HashMap<>();
		
	    for(Element script: scripts) {
	    	
	    	if(script.outerHtml().indexOf("window.WIZ_global_data") == -1) {
	    		continue;
	    	}
	    	
	    	//log.info("[TraslateServiceImpl][findParameters] outerHtml: "+script.outerHtml());
	    				
			String atParam = "";
			String fsid = "";
			String bl="";
						
			//FdrFJe
			Pattern fsidPattern = Pattern.compile("\"FdrFJe\":\"(.*?)\"");
			Matcher fsidMatcher = fsidPattern.matcher(script.outerHtml());
			
			while(fsidMatcher.find()) {
				fsid=fsidMatcher.group(1);
			}
			
			
			//cfb2h
			Pattern blPattern = Pattern.compile("\"cfb2h\":\"(.*?)\"");
			Matcher blMatcher = blPattern.matcher(script.outerHtml());
			
			while(blMatcher.find()) {				
				bl=blMatcher.group(1);
			}
			
			
			//at
			Pattern atPattern = Pattern.compile("\"SNlM0e\":\"(.*?)\"");
			Matcher atMatcher = atPattern.matcher(script.outerHtml());
			
			while(atMatcher.find()) {				
				atParam = atMatcher.group(1);
			}
			
			//log.info("fsid: "+fsid);
			//log.info("bl: "+bl);
			//log.info("at: "+atParam);
			
			result.put("at", atParam);
			result.put("fsid", fsid);
			result.put("bl", bl);
			
			break;		
		}
				
		return result;
	}

	@Override
	public String translate(TranslationRequest traslateRequest) throws GTTS4JException {

		String url = GTTS4JConstant.URL_BASE
				+ String.format(GTTS4JConstant.URL_TRASLATE_BATCH_EXECUTE, 
						traslateRequest.getFSid(), 
						traslateRequest.getBl(),
						traslateRequest.getLang(), 
						traslateRequest.getReqid());
				
		String fReqParam = String.format(GTTS4JConstant.REQUEST_TRASLATE_PARAM_F_REQ_FORMAT,
				traslateRequest.getText(), 
				traslateRequest.getLang(),
				traslateRequest.getLangTranslate());
		
		BatchExecuteRequest batchExecuteRequest=BatchExecuteRequest.builder()
				.url(url)
				.cookie(traslateRequest.getCookie())
				.atParam(traslateRequest.getAtParam())
				.fReqParam(fReqParam)
				.build();
				
		Response response = postBatchExecute(batchExecuteRequest);
		
		return response.body();
	}

	

	@Override
	public Response postBatchExecute(BatchExecuteRequest batchExecuteRequest) throws GTTS4JException {
		
		/*System.out.println("url: "+batchExecuteRequest.getUrl());
		System.out.println(GTTSConstant.HEADER_CONTENT_TYPE+": "+batchExecuteRequest.getContentType());
		System.out.println(GTTSConstant.REQUEST_PARAM_F_REQ+": "+batchExecuteRequest.getFReqParam());
		System.out.println(GTTSConstant.REQUEST_PARAM_AT+" :"+ batchExecuteRequest.getAtParam());
		System.out.println(GTTSConstant.COOKIE_NID+" :"+ batchExecuteRequest.getCookie());*/
		
		Connection.Response res;
		try {
			res = Jsoup.connect(batchExecuteRequest.getUrl())
					.userAgent(batchExecuteRequest.getUserAgent())
					.header(GTTS4JConstant.HEADER_CONTENT_TYPE, batchExecuteRequest.getContentType())
					.data(GTTS4JConstant.REQUEST_PARAM_F_REQ, batchExecuteRequest.getFReqParam())
					.data(GTTS4JConstant.REQUEST_PARAM_AT, batchExecuteRequest.getAtParam())
					.cookie(GTTS4JConstant.COOKIE_NID, batchExecuteRequest.getCookie())
					.method(Method.POST)
					.ignoreContentType(true)
					.execute();
		} catch (IOException e) {
			log.severe(e.getMessage());
			throw new GTTS4JException(e.getMessage());
		}

		if (res.statusCode() != 200) {			
			throw new GTTS4JException("statusCode: " + res.statusCode());
		}

		return res;		
	}
}
