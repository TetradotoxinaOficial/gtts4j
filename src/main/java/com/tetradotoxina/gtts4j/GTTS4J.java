package com.tetradotoxina.gtts4j;

import com.tetradotoxina.gtts4j.exception.GTTS4JException;
import com.tetradotoxina.gtts4j.model.ConfigTranslation;

/**
 * @author tetradotoxina
 *
 */
public interface GTTS4J {

	/**
	 * @param text text to convert to speech the default language is english (en)
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(String text) throws GTTS4JException;

	/**
	 * @param text text to convert to speech the default language is english (en)
	 * @param slow voice speed
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(String text, boolean slow) throws GTTS4JException;

	/**
	 * @param text text to convert to speech
	 * @param lang language of the text to be converted into speech (GTTS4JLang)
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(String text, String lang) throws GTTS4JException;

	/**
	 * @param text text to convert to speech
	 * @param lang language of the text to be converted into speech (GTTS4JLang)
	 * @param slow voice speed
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(String text, String lang, boolean slow) throws GTTS4JException;

	/**
	 * @param text text to convert to speech
	 * @param lang language of the text to be converted into speech (GTTS4JLang)
	 * @param langTranslate language to translate
	 * @param slow voice speed
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(String text, String lang, String langTranslate, boolean slow) throws GTTS4JException;

	/**
	 * @param configTranslation settings to convert text to speech
	 * @return bytes of text-to-speech
	 */
	byte[] textToSpeech(ConfigTranslation configTranslation) throws GTTS4JException;

	/**
	 * Convert bytes to mp3 file
	 * @param filePath mp3 file path
	 * @param bytes bytes of text-to-speech
	 */
	void saveFile(String filePath, byte[] bytes) throws GTTS4JException;

	/**
	 * Convert bytes to mp3 file
	 * @param filePath mp3 file path
	 * @param bytes bytes of text-to-speech
	 * @param overwriteFile set whether mp3 file is overwritten
	 */
	void saveFile(String filePath, byte[] bytes, boolean overwriteFile) throws GTTS4JException;

	/**
	 * @param text text to translate
	 * @param lang language of the text to be translated (GTTS4JLang)
	 * @param langTranslate language to translate (GTTS4JLang)
	 * @return translated text
	 */
	String translate(String text, String lang, String langTranslate) throws GTTS4JException;

	/**
	 * @param configTranslation settings to translate
	 * @return translated text
	 */
	String translate(ConfigTranslation configTranslation) throws GTTS4JException;
}
